#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Metodo
int display(int age){ 
return age; }

//Atributos de la persona
struct Persona{
char *nombre;
int fecha;
int (*display)(int);
}

persona = {"Juan Perez", 31, &display}; //constructor

//crear la estructura
struct traba{
char *trabajo;
struct Persona persona; //simulating inheritance
}worker = {"Programador"};

int main(void){
struct Persona judith = persona; //Instancia
struct traba mariz = worker; //Instancia
              mariz.persona = persona; //Interface

judith.nombre = "Judith Lopez Librado";
judith.fecha = 21;

printf("%s %d\n", judith.nombre, judith.display(judith.fecha));
printf("NOMBRE: %s - %s\n", mariz.persona.nombre, mariz.trabajo);
return 0;
}
